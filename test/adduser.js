var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Add User', function(){

	afterEach(function(done) {
		var db = monk('mongodb://127.0.0.1:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.delete('/users/deleteuser/' + userId)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					db.close();
					done();
				});
		});
	});

	it('Adds a new user with user name \'test user\'', function(done){
		var newUser = {
			'username' : 'test user',
			'email' : 'test1@test.com',
			'fullname' : 'Al Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(200)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			done();
		});
	});
});